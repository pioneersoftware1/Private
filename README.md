# Private

Date of update - 1 Nov 2019

PRIVACY POLICY
Pioneersoftware Private Limited (hereinafter referred to as “Pioneer”, “we”, “us” and “our”) is fully committed to protecting your privacy and has taken all necessary and reasonable measures to protect your information and handle the same in a safe and responsible manner in accordance with the terms set out below.

Pioneer is an Nepal Pvt Limited company and its primary focus is in Nepal. Its services are primarily B2B and individuals are not expected to avail any special services directly. Though, it has no offices anywhere outside Nepal. Hence, this privacy policy is Governed by Nepal Laws

We can collect your personal data

●	When you fill contact form at our website

●	When you subscribe to our newsletter

●	When you use our software solution

●	When you visit our offices

●	When we meet you

We can collect the following data.

●	Name

●	Phone Number

●	Email Address

●	Organisation

●	Designation

●	Address

●	Password

●	In case you work for an organisation who uses our Attendance Management System and/ or HR management system, we may collect some more HR specific information depending on the organisation concerned

Our servers also record information such as browser type, location, operating system, network information, type of mobile device, etc.

Users shall place only authentic data on the website and application and shall review their information for any inaccurate or deficient data. Pioneer may not be able to provide services if you choose not to provide the requisite information.

Cookies

Pioneer may store some information placing cookies on your device. Cookies are pieces of information that an application transfers to the hard drive of a user's computer/ devices for record-keeping purposes. This information facilitates your use of application and ensures that you do not need to re-enter your details every time you visit it. You can erase or choose to block this information if you want to. Erasing or blocking such information may limit the range of features available to the user visitor. Pioneer also uses such information to provide users a personalized experience on the application. Pioneer may utilize such information to allow users to use the application without logging on upon re-entering; and to verify that user meets the criteria required to process their requests
 
Use of Information

We will use your personal data to send you newsletters, marketing offers and information surveys through emails. We will also use your personal data to answer your messages and provide customer support services.

In case you are an application user, the personal data will be used for the administration of the services, to personalise service, to provide customer service, to send notifications etc.

We may also use personal information to conduct research and perform analysis in order to measure, maintain, protect, develop and improve our services.

Pioneer retains complete anonymity in all analytics to third parties. None of the personally sensitive information is used, except in a limited set of circumstances that if it is required to do so by law or in connection with any legal proceedings or prospective legal proceedings, and in order to establish, exercise or defend legal rights.

Pioneer will not transfer the personal data collected to any third party without prior intimation to the data subject.

Links

Pioneer websites and applications may contain links to third party websites to facilitate navigation or to give additional and relevant information. Pioneer’s  Privacy Policy does not apply to practices of such sites that it does not own or control or to people it does not employ. Therefore we are not responsible for the content or the privacy practices employed by organizations sponsoring these other sites. These links include but are not limited to partner sites, client sites etc.

Storage, Access and Security

The information and data is safely stored on the cloud and servers of our service providers and suppliers with restricted access in data centres in Nepal. Only designated employees or associates who need to carry out legitimate business functions are permitted to view your personal data. They are bound by strict confidentiality agreements. Employees or associates who violate this privacy policy are subject to disciplinary actions, up to and including termination. Pioneer  safeguards your privacy using known security standards and procedures and complies with applicable privacy laws. Pioneer takes reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information though it's lifecycle in our infrastructure.

Transfer of Data

Pioneer Services and server service providers have operations in multiple countries, and Pioneer  may need to transfer your information to other countries. However, this Privacy Policy is applicable to all such information transferred between any of the countries in which Pioneer or our server provider operates or servers are located. You agree to such cross-border transfers of your personal information.

Data Retention

We retain a record of your personal information in order to provide you with a high quality and consistent service. We will always retain your personal information in accordance with the General Privacy Principles and never retain your information for longer than is necessary.
 
Your Rights

We wish to emphasize that as a data subject, you have the following rights, which this Policy and our use of personal data have been designed to uphold:

●	The right to be informed about our collection and use of personal data

●	The right of access to the personal data we hold about you

●	The right to rectification of any personal data that is inaccurate or incomplete

●	The right to be forgotten – i.e. the right to ask us to delete any personal data

●	The right to restrict (i.e. prevent) the processing of your personal data

●	The right to data portability

●	The right to object to us using your personal data for particular purposes

●	The right to complain

Changes to the privacy policy

We reserve the right to change this policy at any time. We will notify you by email or by means of a notice on the website of any material changes to this Privacy Policy. Please review this policy periodically, and especially before you provide any Personal Data. Your continued use of the Services after any changes or revisions to this Privacy Policy shall indicate your agreement with the terms of such revised Privacy Policy.

Contact Information

If you have any grievances about this privacy policy or our treatment of your personal information, please write to privacy grievance officer of Mobisy by emailing info@pioneer.com.np or by writing to us at Pioneer software Private Limited, Kathmandu Nepal

By accessing this website and/ or our software application, you hereby consent to the collection, receipt, possession, storage, dealing and handling of your personal information as per this Privacy Policy.
